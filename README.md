# Malokeir@x-eletric@x

imagens/malokeirox eletrikox.png

    - Fase 1. Gerar auto excedente de energia solar em equipamentos sociais
      - Projeto piloto como base para editais regionais por subprefeituras
    - Fase 2. Cursos/Podcasts de energia solar como redução de danos e cultura pop rua
    - Fase 3. Cursos/Podcasts de energia solar como autonomia em foco e cultura pop rua
    - Fase 4. Incremento do pib per capita pop rua
      - Bater Marica RJ, US$100.000,00

## Descrição
Malokeir@x-eletric@x é uma aplicação web para análise de consumo de energia e avaliação do potencial solar com base em imagens. Utiliza técnicas de visão computacional para identificar objetos que consomem energia diretamente nas imagens fornecidas e calcula o potencial solar com base no endereço fornecido.

## Funcionalidades
- Análise de imagens para detecção de objetos relevantes.
- Cálculo do consumo de energia estimado.
- Avaliação do potencial solar com base no consumo e endereço fornecido.
- Estimativa de CAPEX, OPEX, ROI e nova conta de luz.
- Integração com Swagger para documentação da API.

## Tecnologias Utilizadas
- Python
- Flask
- OpenCV
- NumPy
- Pandas
- BeautifulSoup
- Requests
- Flasgger (Swagger para Flask)


## Estrutura do Projeto
```
project_root/
│
├── app/
│   ├── __init__.py
│   ├── app.py
│   ├── analyze.py
│   ├── budget.py
│   ├── calculate.py
│   ├── config.py
│   ├── requirements.txt
│   ├── static/
│   │   ├── css/
│   │   │   └── style.css
│   │   └── images/
│   └── templates/
│       ├── budget.html
│       ├── energy_consumption.html
│       └── solar_potential.html
│
├── swagger/
│   ├── energy_consumption.yml
│   ├── solar_potential.yml
│   └── budget.yml
│
├── tests/
│   ├── test_analyze.py
│   ├── test_budget.py
│   └── test_calculate.py
│
├── venv/  (ambiente virtual Python)
│
├── README.md
└── requirements.txt

```

## Configuração do YOLO
Para utilizar a detecção de objetos com YOLOv3, siga estas instruções:

1. **Baixe `yolov3.weights`**:
   - Baixe o arquivo `yolov3.weights` do seguinte link:
     - [yolov3.weights](https://github.com/patrick013/Object-Detection---Yolov3/raw/master/model/yolov3.weights)
   - Salve o arquivo `yolov3.weights` em um diretório chamado `yolo` no diretório raiz do projeto.

2. **Baixe `yolov3.cfg`**:
   - Visualize o arquivo `yolov3.cfg` no seguinte link:
     - [yolov3.cfg](https://github.com/pjreddie/darknet/blob/master/cfg/yolov3.cfg)
   - Salve o arquivo `yolov3.cfg` no diretório `yolo` que você criou anteriormente.

### Executando a Aplicação
Para executar a aplicação, siga os passos descritos no README.md.

### Uso da API
Documentação da API disponível através do Swagger. Acesse a documentação executando a aplicação e acessando:
- `http://localhost:5000/apidocs`

Explicação
Reconhecimento de Equipamentos: A função detect_objects usa um modelo YOLO pré-treinado para detectar objetos na imagem fornecida. Esta função retorna uma lista de equipamentos reconhecidos.
Cálculo do Consumo de Energia: O endpoint /energy_consumption processa a imagem enviada, reconhece os equipamentos, calcula o consumo total com base em uma base de dados e retorna o resultado.
Cálculo do Potencial Solar: O endpoint /solar_potential usa a geocodificação para obter as coordenadas de um endereço e consulta um serviço externo para obter dados de irradiação solar.
Geração de Orçamento: O endpoint /budget calcula CAPEX e OPEX com base na potência e área do telhado fornecidas, e gera um diagrama em formato Markdown.
Testando os Endpoints
Cálculo de Consumo de Energia:

bash
Copiar código
curl -X POST -F 'image=@/caminho/para/imagem.jpg' http://127.0.0.1:5000/energy_consumption
Cálculo do Potencial Solar:

bash
Copiar código
curl -X POST -d "address=1600 Amphitheatre Parkway, Mountain View, CA" http://127.0.0.1:5000/solar_potential
Geração de Orçamento:

bash
Copiar código
curl -X POST -H "Content-Type: application/json" -d '{"potencia": 10, "area_roof": 100}' http://127.0.0.1:5000/budget

Com essas implementações, você terá um sistema capaz de automatizar o reconhecimento de equipamentos e o cálculo de consumo de energia, além de calcular o potencial solar e gerar orçamentos detalhados.

### Contribuições
Contribuições são bem-vindas! Abra issues e pull requests para melhorias.

### Licença
Este projeto está licenciado sob a [MIT License](https://opensource.org/licenses/MIT).
```