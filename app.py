from flask import Flask, request, jsonify
import cv2
import numpy as np
import pandas as pd
from geopy.geocoders import Nominatim
from flask import Flask, request, jsonify
from flasgger import Swagger

app = Flask(__name__)

# Carregar informações de consumo de energia (base de dados simplificada)
consumo_equipamentos = pd.DataFrame({
    'equipamento': [
        'lamp', 'air conditioner', 'computer', 'tv', 'refrigerator', 'microwave', 'oven',
        'dishwasher', 'coffee maker', 'toaster', 'blender', 'food processor', 'washing machine',
        'dryer', 'vacuum cleaner', 'iron', 'sewing machine', 'cash register', 'POS terminal',
        'commercial oven', 'commercial refrigerator', 'freezer', 'ice maker', 'coffee machine',
        'espresso machine', 'water cooler', 'vending machine', 'security camera', 'fire alarm',
        'smoke detector', 'sprinkler system', 'air conditioner', 'heater', 'fan', 'lighting system',
        'generator', 'power outlet', 'switch', 'circuit breaker', 'transformer', 'cable box',
        'router', 'modem', 'printer', 'scanner', 'fax machine', 'projector', 'speaker',
        'microphone', 'telephone', 'computer monitor', 'server', 'storage device', 'network switch',
        'firewall', 'UPS', 'digital signage', 'point of sale system', 'security system',
        'access control system', 'building automation system', 'industrial robot', 'conveyor belt',
        'forklift', 'crane', 'welding machine', 'manufacturing equipment', 'medical equipment',
        'laboratory equipment', 'scientific equipment', 'agricultural equipment', 'construction equipment',
        'transportation equipment', 'aerospace equipment', 'military equipment', 'energy equipment',
        'environmental equipment', 'communication equipment', 'entertainment equipment',
        'retail equipment', 'office equipment', 'education equipment', 'sports equipment',
        'recreational equipment'
    ],
    'consumo_medio_watts': [
        10, 1500, 300, 100, 150, 1200, 2000, 1800, 1000, 800, 600, 700, 500, 4500, 200, 1200, 500,
        300, 150, 5000, 1000, 700, 800, 1000, 1200, 100, 120, 30, 20, 10, 50, 1500, 2000, 50, 60,
        8000, 5, 10, 20, 50, 30, 15, 10, 600, 300, 200, 500, 100, 10, 5, 60, 300, 100, 50, 100, 200,
        300, 500, 400, 300, 2000, 1500, 5000, 1000, 12000, 8000, 10000, 12000, 1500, 20000, 500, 7000,
        1000, 300, 200, 1000, 200, 500, 400, 300, 200, 100, 50
    ]
})

geolocator = Nominatim(user_agent="solar_potential_app")

def get_coordinates(address):
    location = geolocator.geocode(address)
    if location:
        return location.latitude, location.longitude
    return None, None

def get_solar_irradiance(lat, lon):
    # Exemplo de URL para obter dados de irradiação solar do NREL
    api_url = f"https://developer.nrel.gov/api/solar/solar_resource/v1.json?api_key=DEMO_KEY&lat={lat}&lon={lon}"
    response = requests.get(api_url)
    if response.status_code == 200:
        data = response.json()
        irradiance = data['outputs']['avg_dni']['annual']
        return irradiance
    return None

# Função para detectar e reconhecer objetos nas imagens
def detect_objects(image_path):
    # Carregar o modelo pré-treinado (exemplo com YOLO)
    net = cv2.dnn.readNet("yolov3.weights", "yolov3.cfg")
    layer_names = net.getLayerNames()
    output_layers = [layer_names[i[0] - 1] for i in net.getUnconnectedOutLayers()]

    # Carregar imagem
    img = cv2.imread(image_path)
    height, width, channels = img.shape

    # Pré-processar a imagem
    blob = cv2.dnn.blobFromImage(img, 0.00392, (416, 416), (0, 0, 0), True, crop=False)
    net.setInput(blob)
    outs = net.forward(output_layers)

    class_ids = []
    confidences = []
    boxes = []

    for out in outs:
        for detection in out:
            scores = detection[5:]
            class_id = np.argmax(scores)
            confidence = scores[class_id]
            if confidence > 0.5:
                # Obter coordenadas da caixa delimitadora
                center_x = int(detection[0] * width)
                center_y = int(detection[1] * height)
                w = int(detection[2] * width)
                h = int(detection[3] * height)

                x = int(center_x - w / 2)
                y = int(center_y - h / 2)

                boxes.append([x, y, w, h])
                confidences.append(float(confidence))
                class_ids.append(class_id)

    indexes = cv2.dnn.NMSBoxes(boxes, confidences, 0.5, 0.4)

    # Lista de classes (equipamentos reconhecidos)
    with open("coco.names", "r") as f:
        classes = [line.strip() for line in f.readlines()]

    recognized_equipments = []

    for i in range(len(boxes)):
        if i in indexes:
            label = str(classes[class_ids[i]])
            recognized_equipments.append(label)

    return recognized_equipments

@app.route('/energy_consumption', methods=['POST'])
def calculate_energy_consumption():
    # Verificar se uma imagem foi enviada
    if 'image' not in request.files:
        return jsonify({'error': 'No image provided'}), 400
    
    image = request.files['image']
    image_path = f"./{image.filename}"
    image.save(image_path)

    # Detectar e reconhecer equipamentos na imagem
    equipamentos = detect_objects(image_path)
    
    total_consumo = 0
    
    for equipamento in equipamentos:
        consumo = consumo_equipamentos[consumo_equipamentos['equipamento'] == equipamento]
        if not consumo.empty:
            total_consumo += consumo['consumo_medio_watts'].values[0]
    
    return jsonify({'total_consumo_watts': total_consumo, 'equipamentos_reconhecidos': equipamentos})

@app.route('/solar_potential', methods=['POST'])
def calculate_solar_potential():
    address = request.form['address']
    lat, lon = get_coordinates(address)
    if not lat or not lon:
        return jsonify({'error': 'Unable to geocode address'}), 400

    irradiance = get_solar_irradiance(lat, lon)
    if not irradiance:
        return jsonify({'error': 'Unable to get solar irradiance data'}), 500

    # Para simplificar, vamos assumir uma área de telhado padrão de 100 m² e eficiência de 15%
    area_roof = 100.0
    efficiency = 0.15
    potential = area_roof * irradiance * efficiency / 1000  # Convertendo para kW

    return jsonify({
        'potential_kW': potential,
        'area_roof_m2': area_roof,
        'latitude': lat,
        'longitude': lon,
        'address': address
    })

@app.route('/budget', methods=['POST'])
def generate_budget():
    # Dados de entrada para o orçamento
    potencia = request.json.get('potencia', 0)
    area_roof = request.json.get('area_roof', 0)

    # Supondo CAPEX e OPEX padrão por kW
    capex_per_kw = 1500.0
    opex_per_kw = 75.0

    capex = potencia * capex_per_kw
    opex = potencia * opex_per_kw

    # Lista de equipamentos (simplificada)
    equipamentos = [
        {"nome": "Inversor", "quantidade": 1, "fabricante": "FabrInc", "potencia": potencia, "eficiencia": 0.98},
        {"nome": "Painel Solar", "quantidade": int(area_roof * 0.3), "fabricante": "SolarCorp", "potencia": 300, "eficiencia": 0.18},
        {"nome": "Cabos", "quantidade": 200, "fabricante": "CableInc", "potencia": None, "eficiencia": None},
        {"nome": "Conectores", "quantidade": 66, "fabricante": "ConnInc", "potencia": None, "eficiencia": None},
        {"nome": "Dispositivos de Segurança", "quantidade": 5, "fabricante": "SafeTech", "potencia": None, "eficiencia": None}
    ]

    # Gerar diagrama em formato Markdown
    diagrama_md = "# Diagrama Elétrico e Executivo\n\n## Equipamentos\n\n| Nome                   | Quantidade | Fabricante | Potência (W) | Eficiência |\n|------------------------|------------|------------|--------------|------------|\n"
    for equipamento in equipamentos:
        diagrama_md += f"| {equipamento['nome']} | {equipamento['quantidade']} | {equipamento['fabricante']} | {equipamento['potencia'] if equipamento['potencia'] else '-'} | {equipamento['eficiencia'] if equipamento['eficiencia'] else '-'} |\n"
    diagrama_md += "\n## Diagramas\n\n### Diagrama Elétrico\n\n![Diagrama Elétrico](url_para_diagrama_eletrico.png)\n\n### Diagrama Executivo\n\n![Diagrama Executivo](url_para_diagrama_executivo.png)\n"

    return jsonify({
        'capex': capex,
        'opex': opex,
        'equipamentos': equipamentos,
        'diagrama_md': diagrama_md
    })

if __name__ == '__main__':
    app.run(debug=True)
