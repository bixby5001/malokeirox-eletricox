import unittest
from app.calculate import calculate_consumption_and_solar, calculate_financials

class TestCalculate(unittest.TestCase):

    def test_calculate_consumption_and_solar(self):
        # Testando o cálculo de consumo de energia e potencial solar
        mock_results = [['lamp', 'computer'], ['air conditioner']]
        mock_context = 'mock_context'
        consumo_total, potencial_solar, area_telhado = calculate_consumption_and_solar(mock_results, mock_context)
        
        # Verificando se os resultados são números e maiores que zero
        self.assertIsInstance(consumo_total, (int, float))
        self.assertGreater(consumo_total, 0)
        
        self.assertIsInstance(potencial_solar, (int, float))
        self.assertGreater(potencial_solar, 0)
        
        self.assertIsInstance(area_telhado, (int, float))
        self.assertGreater(area_telhado, 0)

    def test_calculate_financials(self):
        # Testando o cálculo de CAPEX, OPEX, ROI e nova conta de luz
        consumo_total = 1000
        potencial_solar = 100
        capex, opex, roi, nova_conta_luz = calculate_financials(consumo_total, potencial_solar)
        
        # Verificando se os resultados são números e maiores que zero
        self.assertIsInstance(capex, (int, float))
        self.assertGreater(capex, 0)
        
        self.assertIsInstance(opex, (int, float))
        self.assertGreater(opex, 0)
        
        self.assertIsInstance(roi, (int, float))
        self.assertGreater(roi, 0)
        
        self.assertIsInstance(nova_conta_luz, (int, float))
        self.assertGreater(nova_conta_luz, 0)

if __name__ == '__main__':
    unittest.main()
