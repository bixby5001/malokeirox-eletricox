import unittest
from app.budget import calculate_budget

class TestBudget(unittest.TestCase):

    def test_calculate_budget(self):
        # Testando o cálculo do orçamento com valores fictícios
        potencia = 10
        area_roof = 100
        resultado = calculate_budget(potencia, area_roof)
        
        # Verificando se os resultados esperados são retornados
        self.assertIsInstance(resultado, dict)
        self.assertIn('capex', resultado)
        self.assertIn('opex', resultado)
        self.assertIn('lista_equipamentos', resultado)
        self.assertIn('inversor', resultado)
        self.assertIn('paineis', resultado)
        self.assertIn('fabricante', resultado)
        self.assertIn('eficiencia', resultado)
        self.assertIn('cabos', resultado)
        self.assertIn('conectores', resultado)
        self.assertIn('dispositivos_seguranca', resultado)

if __name__ == '__main__':
    unittest.main()
