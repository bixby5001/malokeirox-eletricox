import unittest
from unittest.mock import patch, MagicMock
from app.analyze import analyze_image

class TestAnalyze(unittest.TestCase):

    @patch('cv2.dnn.readNet')
    @patch('cv2.dnn.blobFromImage')
    @patch('cv2.dnn.NMSBoxes')
    def test_analyze_image(self, mock_nmsboxes, mock_blobfromimage, mock_readnet):
        # Configuração dos mocks
        mock_net = MagicMock()
        mock_readnet.return_value = mock_net
        mock_blobfromimage.return_value = MagicMock()
        mock_nmsboxes.return_value = [[0]]

        # Simulando uma imagem válida para teste
        mock_img_path = "/path/to/image.jpg"

        # Chamando a função de análise de imagem
        result = analyze_image(mock_img_path)

        # Verificando se a função retorna um dicionário com as chaves esperadas
        self.assertIsInstance(result, dict)
        self.assertIn('count_persons', result)
        self.assertIn('count_pets', result)
        self.assertIn('total_consumption_watts', result)

        # Verificando o comportamento da função em casos específicos
        # Testar se a função retorna valores corretos para uma imagem simulada
        self.assertEqual(result['count_persons'], 1)  # Exemplo: 1 pessoa detectada
        self.assertEqual(result['count_pets'], 0)     # Exemplo: 0 pets detectados
        self.assertEqual(result['total_consumption_watts'], 1500)  # Exemplo: Consumo total estimado

if __name__ == '__main__':
    unittest.main()
