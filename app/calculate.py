"""
calculate.py

Data: 20-06-2024
Versão: 1.0

Descrição:
Este arquivo contém funções para cálculos de consumo de energia e potencial solar.
"""

import pandas as pd

# Carregar informações de consumo de energia (base de dados simplificada)
consumo_equipamentos = pd.DataFrame({
    'equipamento': ['air conditioner', 'computer', 'tv', 'refrigerator', 'washing machine', 'dryer', 'dishwasher', 
                    'oven', 'toaster', 'blender', 'coffee maker', 'vacuum cleaner', 'laptop', 'printer', 'tablet', 
                    'phone', 'radio', 'speaker', 'game console', 'stereo', 'projector', 'dvd player', 'heater', 
                    'fan', 'electric grill', 'food processor', 'mixer', 'juicer', 'hair drier', 'shower head', 
                    'lawn mower', 'water pump', 'electric fence', 'electric irrigator', 'charger', 'power strip'],
    'consumo_medio_watts': [1500, 300, 100, 100, 500, 1500, 1000, 2500, 1000, 500, 1000, 1000, 50, 10, 10, 10, 50, 
                            100, 100, 100, 100, 100, 1500, 1000, 100, 1500, 500, 500, 500, 1000, 100, 500, 500, 100, 
                            10, 10]
})

def calculate_consumption_and_solar(detected_objects, context):
    """
    Calcula o consumo de energia e o potencial solar baseado nos objetos detectados.

    Args:
        detected_objects (list): Lista de objetos detectados na imagem.
        context (str): Contexto do projeto.

    Returns:
        tuple: Tupla com o consumo total em kWh, o potencial solar em kW,
               e a área do telhado em m².
    """
    # Contagem de pessoas e pets
    total_pessoas = detected_objects.count('pessoa')
    total_pets = detected_objects.count('pet')

    # Filtrar equipamentos detectados para calcular o consumo
    equipamentos_detectados = [objeto for objeto in detected_objects if objeto in consumo_equipamentos['equipamento'].values]

    # Calcular o consumo total de energia
    consumo_total = sum(consumo_equipamentos[consumo_equipamentos['equipamento'].isin(equipamentos_detectados)]['consumo_medio_watts']) / 1000

    # Calcular o potencial solar baseado no número de pessoas
    potencial_solar = total_pessoas * 1.5  # Exemplo simples de cálculo de potencial solar

    # Calcular área do telhado baseado no contexto
    area_telhado = 100  # Exemplo simples de cálculo de área do telhado

    return consumo_total, potencial_solar, area_telhado

def calculate_financials(consumo_total, potencial_solar):
    """
    Calcula CAPEX, OPEX, ROI e a nova conta de luz.

    Args:
        consumo_total (float): Consumo total em kWh.
        potencial_solar (float): Potencial solar em kW.

    Returns:
        tuple: Tupla com CAPEX, OPEX, ROI e nova conta de luz.
    """
    # Exemplo simplificado de cálculo de CAPEX, OPEX, ROI e nova conta de luz
    capex = 10000
    opex = 5000
    roi = (potencial_solar * 365 * 0.15) - (consumo_total * 0.25)
    nova_conta_luz = consumo_total * 0.15

    return capex, opex, roi, nova_conta_luz

