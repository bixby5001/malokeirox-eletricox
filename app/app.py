"""
app.py

Data: 20-06-2024
Versão: 1.0

Descrição:
Este arquivo contém a configuração principal do Flask para a aplicação de cálculo
de consumo de energia, potencial solar e geração de orçamento.
"""

from flask import Flask, request, jsonify
from flasgger import Swagger
from flasgger.utils import swag_from
from werkzeug.utils import secure_filename
from app.analyze import analyze_image
from app.calculate import calculate_consumption_and_solar, calculate_financials
import os
import pandas as pd

app = Flask(__name__)
swagger = Swagger(app)

# Configuração para upload de arquivos
UPLOAD_FOLDER = 'uploads'
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg'}

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER

# Carregar informações de consumo de energia (base de dados simplificada)
consumo_equipamentos = pd.DataFrame({
    'equipamento': [
        'lamp', 'air conditioner', 'computer', 'tv', 'refrigerator', 'microwave', 'oven',
        'dishwasher', 'coffee maker', 'toaster', 'blender', 'food processor', 'washing machine',
        'dryer', 'vacuum cleaner', 'iron', 'sewing machine', 'cash register', 'POS terminal',
        'commercial oven', 'commercial refrigerator', 'freezer', 'ice maker', 'coffee machine',
        'espresso machine', 'water cooler', 'vending machine', 'security camera', 'fire alarm',
        'smoke detector', 'sprinkler system', 'air conditioner', 'heater', 'fan', 'lighting system',
        'generator', 'power outlet', 'switch', 'circuit breaker', 'transformer', 'cable box',
        'router', 'modem', 'printer', 'scanner', 'fax machine', 'projector', 'speaker',
        'microphone', 'telephone', 'computer monitor', 'server', 'storage device', 'network switch',
        'firewall', 'UPS', 'digital signage', 'point of sale system', 'security system',
        'access control system', 'building automation system', 'industrial robot', 'conveyor belt',
        'forklift', 'crane', 'welding machine', 'manufacturing equipment', 'medical equipment',
        'laboratory equipment', 'scientific equipment', 'agricultural equipment', 'construction equipment',
        'transportation equipment', 'aerospace equipment', 'military equipment', 'energy equipment',
        'environmental equipment', 'communication equipment', 'entertainment equipment',
        'retail equipment', 'office equipment', 'education equipment', 'sports equipment',
        'recreational equipment'
    ],
    'consumo_medio_watts': [
        10, 1500, 300, 100, 150, 1200, 2000, 1800, 1000, 800, 600, 700, 500, 4500, 200, 1200, 500,
        300, 150, 5000, 1000, 700, 800, 1000, 1200, 100, 120, 30, 20, 10, 50, 1500, 2000, 50, 60,
        8000, 5, 10, 20, 50, 30, 15, 10, 600, 300, 200, 500, 100, 10, 5, 60, 300, 100, 50, 100, 200,
        300, 500, 400, 300, 2000, 1500, 5000, 1000, 12000, 8000, 10000, 12000, 1500, 20000, 500, 7000,
        1000, 300, 200, 1000, 200, 500, 400, 300, 200, 100, 50
    ]
})

def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

# Definindo endpoints

@app.route('/energy_consumption', methods=['POST'])
@swag_from('../swagger/energy_consumption.yml')
def energy_consumption():
    if 'image' not in request.files:
        return jsonify({'error': 'No file part'}), 400

    file = request.files['image']

    if file.filename == '':
        return jsonify({'error': 'No selected file'}), 400

    if file and allowed_file(file.filename):
        filename = secure_filename(file.filename)
        filepath = os.path.join(app.config['UPLOAD_FOLDER'], filename)
        file.save(filepath)
        detected_objects = analyze_image(filepath)
        consumo_total, potencial_solar, area_telhado = calculate_consumption_and_solar(detected_objects, 'default_context')
        return jsonify({
            'consumo_total_kwh': consumo_total,
            'potencial_solar_kw': potencial_solar,
            'area_telhado_m2': area_telhado
        }), 200
    else:
        return jsonify({'error': 'File type not allowed or file upload failed'}), 400

@app.route('/solar_potential', methods=['POST'])
@swag_from('../swagger/solar_potential.yml')
def solar_potential():
    address = request.form.get('address')
    # Lógica para calcular potencial solar baseado no endereço
    return jsonify({
        'address': address,
        'potencial_solar': 'To be calculated'
    }), 200

@app.route('/budget', methods=['POST'])
@swag_from('../swagger/budget.yml')
def budget():
    data = request.get_json()
    potencia = data.get('potencia')
    area_roof = data.get('area_roof')
    # Lógica para geração de orçamento
    return jsonify({
        'potencia': potencia,
        'area_roof': area_roof,
        'capex': 'To be calculated',
        'opex': 'To be calculated',
        'lista_equipamentos': ['To be determined'],
        'inversor': 'To be determined',
        'paineis': 'To be determined',
        'fabricante': 'To be determined',
        'eficiencia': 'To be determined',
        'cabos': 'To be determined',
        'conectores': 'To be determined',
        'dispositivos_seguranca': 'To be determined'
    }), 200

if __name__ == '__main__':
    app.run(debug=True)

